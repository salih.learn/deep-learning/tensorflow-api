import tensorflow as tf
from tensorflow import keras
import numpy as np

num_features = 3
num_rows = 2
x_train = np.random.random((num_rows, num_features))
y_train = np.random.random((num_features,))

input_layer = keras.Input((num_features, ))
layer = keras.layers.Dense(1)(input_layer)
model = keras.Model(input_layer, layer)

print(x_train)
print(model.predict(x_train))
