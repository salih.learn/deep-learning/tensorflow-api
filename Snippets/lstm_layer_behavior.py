import tensorflow as tf
from tensorflow import keras
import numpy as np

batches = 2
timesteps = 3
features = 1
output_features = 1
x_train = np.random.random((batches, timesteps, features))

print("Without return sequences set to true")
print("------------------------------------")
input_layer = keras.Input((timesteps, features))
layer = keras.layers.LSTM(output_features)(input_layer)
model = keras.Model(input_layer, layer)
print("Prediction input shape: ", x_train.shape)
print("Prediction output shape: ", model.predict(x_train).shape)

print("\n")
print("With return sequences set to true")
print("------------------------------------")
input_layer = keras.Input((timesteps, features))
layer = keras.layers.LSTM(output_features, return_sequences=True)(input_layer)
model = keras.Model(input_layer, layer)
print("Prediction input shape: ", x_train.shape)
print("Prediction output shape: ", model.predict(x_train).shape)