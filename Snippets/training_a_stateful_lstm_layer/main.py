"""

Author:     Muhammed Salih
Date:       29-04-2020

This script uses a stateful recurrent neural network to predict the next number 
in a given sequence

A script strictly meant for illustration purposes. Best practices are ignored.
For instance, the model is trained on the a dataset multiple times leading to overfitting,
and prediction is done on the same datset used in training.

This script does not produce stable results.

"""

import tensorflow as tf
from tensorflow import keras
import numpy as np

"""
Dataset Details
---------------
    Shape: (2, 5, 30, 1)
    Description: 
        Inputs and labels of a batch of 5 items each containing 30 timesteps of a single feature
        If an item in x is [1, 2, .....], corresponding item in y will be [2, 3, ...] with the same length
"""
dsname = 'dataset.npy'
ds = np.load(dsname)
x = ds[0]
y = ds[1]
(batch_size, timesteps, input_feats) = x.shape
print(batch_size, timesteps, input_feats)
output_feats = y.shape[2]

input_layer = keras.Input(batch_shape=(batch_size, None, input_feats))
one_to_one_rnn_layer = keras.layers.LSTM(output_feats, return_sequences=True, activation='relu', stateful=True)(input_layer)
model = keras.Model(input_layer, one_to_one_rnn_layer)

model.compile(
    loss=keras.losses.MeanSquaredError(),
    optimizer=keras.optimizers.RMSprop(),
    metrics=['acc']
)

model.fit(x, y, epochs=10, batch_size=5)
print(np.floor(model.predict(x)))