"""
Author:     Muhammed Salih 
            - (gitlab.com/salih.four)

Date:       29-04-2020

"""

import tensorflow as tf
from tensorflow import keras
import numpy as np 

# Custom RNN Cell
class RNNCell(keras.layers.Layer):
    def __init__(self, units, *args, **kwargs):
        self.units = units
        print("Rnn cell units = ", self.units)
        self.state_size = units
        super(RNNCell, self).__init__(*args, **kwargs)

    def build(self, input_shape):
        self.activation_layer = keras.layers.Dense(self.units, activation=None)
        self.output_layer = keras.layers.Dense(self.units, activation=None)

    def call(self, inputs, states):
        # keras.backend.print_tensor(inputs, message='Inputs = ')
        # keras.backend.print_tensor(states, message='States = ')
        activations = self.activation_layer(inputs)
        output = self.output_layer(activations)
        return output, activations

# Getting sample dataset
"""
Dataset Details
---------------
    Shape: (2, 5, 30, 1)
    Description: 
        Inputs and labels of a batch of 5 items each containing 30 timesteps of a single feature
        If an item in x is [1, 2, .....], corresponding item in y will be [2, 3, ...] with the same length
"""
dsname = 'dataset.npy'
ds = np.load(dsname)
x = ds[0]
y = ds[1]
print(x)
(batch_size, timesteps, input_feats) = x.shape
print(batch_size, timesteps, input_feats)
output_feats = y.shape[2]

input_layer = keras.Input(batch_shape=(1, None, input_feats))
one_to_one_rnn_layer = keras.layers.RNN(RNNCell(1), return_sequences=True)(input_layer)
model = keras.Model(input_layer, one_to_one_rnn_layer)

model.compile(
    loss=keras.losses.MeanSquaredError(),
    optimizer=keras.optimizers.RMSprop(),
    metrics=['acc']
)

model.fit(x, y, epochs=30, batch_size=1)
print(np.floor(model.predict(x[0:1, :, :], batch_size=1)))
# print(np.floor(model.predict(x[1:2, :, :], batch_size=1)))
# print(np.floor(model.predict(x[2:3, :, :], batch_size=1)))