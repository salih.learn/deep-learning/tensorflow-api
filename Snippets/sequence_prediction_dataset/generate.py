import numpy as np 

batch_size = 5
timesteps = 31
input_feats = 1
output_feats = 1
x = np.arange(155).reshape((batch_size, timesteps, input_feats))
y = x[:, 1:, :]
x = x[:, :-1, :]
ds = np.stack([x, y])

# Saving data
"""
    Shape: (2, 5, 30, 1)
    Description: 
        Inputs and labels of a batch of 5 items each containing 30 timesteps of a single feature
        If an item in x is [1, 2, .....], corresponding item in y will be [2, 3, ...]
"""
dsname = 'dataset.npy'
np.save(dsname, ds)

# Test loading data
ds = np.load(dsname)
print(ds.shape)